//! Rust bindings for the graphics API [Vulkan](https://www.khronos.org/vulkan/) 1.0 by the
//! [Khronos group](https://www.khronos.org/).
//!
//! The bindings are genarated from the specification defined in the project
//! [Vulkan-Docs](https://github.com/KhronosGroup/Vulkan-Docs) on [GitHub](https://github.com/).
//!
//! # Usage
//!
//! ```ignore
//! # use std::ffi::CStr;
//! # use std::os::raw::c_void;
//! use std::mem;
//! use visuv::*;
//!
//! fn main() {
//!     let cmd_exported = CommandsExported::load(|name| get_exported(name));
//!     let cmd_global = CommandsGlobal::load(|name| unsafe {
//!         mem::transmute(cmd_exported.GetInstanceProcAddr(0, name.as_ptr())) });
//!     ...
//! }
//! # fn get_exported(name: &CStr) -> *const c_void {
//! #     use std::ptr;
//! #     ptr::null_mut()
//! # }
//! ```
//!
//! # Generation
//!
//! Types specified in the registry (the API specification) and automatically generated are
//!
//! * Base types
//! * Handles
//! * Constants
//! * Enumerations
//! * Bit masks
//! * Function pointers
//! * Structures
//! * Commands
//!
//! ## General name mappings
//!
//! Since all types, names etc. are situated in a separate namespace (crate) prefixes aren't needed
//! and therefore stripped. This applies to all prefixes
//!
//! * vk
//! * Vk
//! * VK_
//!
//! Names reserved in Rust are mapped as listed...
//!
//! | C    | Rust |
//! |------|------|
//! | type | ty   |
//!
//! ## C type mappings
//!
//! Basic C types used in the API specification are mapped to Rust types as listed...
//!
//! | C        | Rust                 | Remarks            |
//! |----------|----------------------|--------------------|
//! | int8_t   | i8                   |                    |
//! | uint8_t  | u8                   |                    |
//! | int16_t  | i16                  |                    |
//! | uint16_t | u16                  |                    |
//! | int32_t  | i32                  |                    |
//! | uint32_t | u32                  |                    |
//! | int64_t  | i64                  |                    |
//! | uint64_t | u64                  |                    |
//! | size_t   | usize                |                    |
//! | int      | i32                  |                    |
//! | float    | f32                  |                    |
//! | char     | std::os::raw::c_char |                    |
//! | void     | std::os::raw::c_void | Only for pointers. |
//!
//! ## Base types
//!
//! In the registry base types are defined using _type_ elements with the category _basetype_.
//! Base types even in the C API headers are just _typedef_, that's what they are generated as
//! in the Rust bindings as well.
//!
//! ## Handles
//!
//! In the registry handles are defined using _type_ elements with the category _handle_.
//! In the C API headers handles are defined using macros. In the Rust bindings they are generated
//! as _type_ definition.
//!
//! Handles are generated as listed...
//!
//! | C                                 | Rust  |
//! |-----------------------------------|-------|
//! | VK_DEFINE_HANDLE                  | usize |
//! | VK_DEFINE_NON_DISPATCHABLE_HANDLE | u64   |
//!
//! ## Constants
//!
//! Constant in the registry are defined a single _enums_ element withut category but the name
//! _API Constants_.
//! In the C API headers they are defined using _#define_. In the Rust binding they are generated
//! as _type_ definition.
//!
//! Constants are generated as...
//!
//! * Values ending with suffix _f_ as Rust type `f32` (e.g. 1000.0f)
//! * Values ending with suffix _ULL_ as Rust type `u64` (e.g. ~0ULL)
//! * Values ending with suffix _U_ or none as Rust type `u32` (e.g. ~0U)
//! * Values with prefix _~_ are negated in Rust with `!`
//!
//! ## Enumerations
//!
//! In the registry enumerations are defined in two steps. First is as type definition using
//! _type_ elements with the category _enum_ for defining the type itself. Second is enumerating
//! the values using the _enums_ element of type _enum_ with an _enum_ element for each value.
//! In the C API headers they are defined as _typedef enum_. In the Rust bindings they are either
//! generated as `#[repr(u32)]` _enum_ or _type_ definition if no values are defined.
//!
//! The name of an enumeration is only stripped of its prefix. The names of the values / variatnts
//! are mapped...
//!
//! 1. Convert to pascal case. The optional tag suffix remains untouched.
//! 1. Remove the name of the enumeration (prefix).
//! 1. Add the prefix _v_ for value. This is also necessary for names which begin with a digit
//!    after removing the prefix.
//!
//! ### Example
//!
//! The enumeration name _VkDisplayPowerStateEXT_ is generated as `DisplayPowerStateEXT`.
//! The value name *VK_DISPLAY_POWER_STATE_OFF_EXT* is generated as `vOffEXT`.
//!
//! ```
//! #[derive(Clone, Copy, Debug, PartialEq)]
//! #[repr(C)]
//! pub enum DisplayPowerStateEXT {
//!     vOffEXT = 0,
//!     vSuspendEXT = 1,
//!     vOnEXT = 2,
//! }
//! ```
//!
//! ## Bit masks
//!
//! Bit masks in the registry are defined as two parts. First part is the mask type defined
//! using _type_ elements with the category _bitmask_ and ending with the suffix _Flags_
//! (without tag). The second part is the bits and values defined in two steps, the type definition
//! using _type_ elements with the category _enum_ and enumerating the bits and values using the
//! _enums_ element of type _bitmask_ with an _enum_ element for each bit and value.
//! In the C API headers they are defined as _typedef_ for the mask part and _typedef enum_ for the
//! bits and values part.
//!
//! In the Rust binding bit masks are generated using the [bitmask](../bitmask/index.html) crate
//! and [bitmask!](../bitmask/macro.bitmask.html) macro.
//!
//! The name of both the mask part and the enumeration part are only stripped of their prefix.
//! The names of the bits / values are mapped...
//!
//! 1. Convert to pascal case. The optional tag suffix remains untouched.
//! 1. Remove the name of the enumeration (prefix).
//! 1. Remove the _Bit_ part (suffix before optional tag suffix).
//! 1. Add the prefix _b_ for bit positions and _v_ for combined values. This is also necessary
//!    for names wich begin with a digit after removing the prefix and makes it easier to
//!    distinguish between bits and values.
//!
//! ### Example
//!
//! The bit mask name _VkCullModeFlags_ is generated as `CullModeFlags`. The enumeration name
//! _VkCullModeFlagBits_ is generated as `CullModeFlagBits`. The bit name *VK_CULL_MODE_FRONT_BIT*
//! is generated as `bFront` as is the value name *VK_CULL_MODE_FRONT_AND_BACK* generated as
//! `vFrontAndBack`.
//!
//! ```
//! # #[macro_use] extern crate bitmask; fn main() {
//! bitmask! {
//!     #[repr(C)]
//!     pub mask CullModeFlags: u32 where flags CullModeFlagBits {
//!         vNone = 0x0,
//!         bFront = 0x1,
//!         bBack = 0x2,
//!         vFrontAndBack = 0x3,
//!     }
//! }
//! # }
//! ```
//!
//! ## Function pointers
//!
//! In the registry function pointer are defined using _type_ elements with the category
//! _funcpointer_. In the Rust bindings they are generated the same as in the C API headers.
//!
//! The names of function pointers aren't changed.
//!
//! ## Structures
//!
//! In the registry structures are defined as _type_ elements with the category _struct_ and a
//! _member_ element for each member. In the C API headers structures are defined as
//! _typedef struct_. In the Rust binding they are generated as _struct_.
//!
//! ## Commands
//!
//! Commands in the registry are defined using _command_ elements. In the C API headers
//! commands are defined as function pointer _typedef_ and optionally as prototype. In the Rust
//! binding commands are generated as _struct_ member defining the signature and holding the pointer
//! to the command.
//!
//! In the Vulkan API command pointers might differ for different instances depending on layers
//! and extensions as well as for different devices depending on vendor as well as extensions
//! activated. For that reason commands are grouped into different categories (structures holding
//! the pointers).
//!
//! | Category | Description |
//! |----------|-------------|
//! | Exported | Exported by the Vulkan loader library. |
//! | Global   | Independent of the instance obtained using _vkGetInstanceProcAddr_ with _0_ as instance. |
//! | Instance | Depending on the instance obtained using _vkGetInstanceProcAddr_. |
//! | Device   | Depending on the device obtained using _vkGetDeviceProcAddr_. |
//!
//! The structures generated are prefixed with _Commands_, contain a field and a method for each
//! command. Those structure also have an associated _load_ function taking a _FnMut_ function as
//! argument returning a pointer (`*const c_void`) for a given command name (argument).
//!
//! ```ignore
//! pub struct CommandsExported {
//!     pub GetInstanceProcAddr: extern "system" fn(Instance, *const c_char)
//!         -> PFN_vkVoidFunction,
//! }
//! impl CommandsExported {
//!     pub fn load<F>(mut f: F) -> CommandsExported
//!         where F: FnMut(&CStr) -> *const c_void
//!     {
//!         ...
//!     }
//!     #[inline]
//!     pub unsafe fn GetInstanceProcAddr(&self, instance: Instance,
//!         pName: *const c_char) -> PFN_vkVoidFunction {
//!         (self.GetInstanceProcAddr)(instance, pName)
//!     }
//! }
//! ```
//!

#[macro_use]
extern crate bitmask;

#[cfg(target_family = "unix")]
extern crate wayland_client;

#[cfg(target_family = "unix")]
extern crate x11_dl;

#[cfg(target_family = "windows")]
extern crate winapi;

#[macro_use]
mod macros;
mod vk_version;
mod vk_platform;
mod vk_union;
mod vk;

pub use vk::*;
pub use vk_union::*;
pub use vk_version::*;
