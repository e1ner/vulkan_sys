#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use std::ffi::{CStr, CString};
use std::fmt;
use std::mem;
use std::os::raw::{c_char, c_void};
use vk_platform::*;
use vk_union::*;

include!(concat!(env!("OUT_DIR"), "/vk.generated.rs"));
