use std::cmp::Ordering;
use std::fmt;

/// Structure containing the parts of a version defined in the Vulkan API.
///
/// Replaces the C macros
///
/// * `VK_MAKE_VERSION`
/// * `VK_VERSION_MAJOR`
/// * `VK_VERSION_MINOR`
/// * `VK_VERSION_PATCH`
///
/// # Examples
///
/// ```
/// use vulkan_sys::Version;
///
/// assert_eq!(Version { major: 1, minor: 1, patch: 1 }, 4198401u32.into());
/// assert_eq!(8396802u32, Version { major: 2, minor: 2, patch: 2 }.into());
/// ```
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Version {
    pub major: u16,
    pub minor: u16,
    pub patch: u16,
}

impl fmt::Debug for Version {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}

impl fmt::Display for Version {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, fmt)
    }
}

impl PartialOrd for Version {
    #[inline]
    fn partial_cmp(&self, other: &Version) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Version {
    fn cmp(&self, other: &Version) -> Ordering {
        match self.major.cmp(&other.major) {
            Ordering::Equal => (),
            o => return o,
        }
        match self.minor.cmp(&other.minor) {
            Ordering::Equal => (),
            o => return o,
        }
        self.patch.cmp(&other.patch)
    }
}

impl Default for Version {
    fn default() -> Self {
        Version { major: 1, minor: 0, patch: 0 }
    }
}

impl From<u32> for Version {
    fn from(version: u32) -> Self {
        Version {
            major: (version >> 22) as u16,
            minor: ((version >> 12) & 0x03ff) as u16,
            patch: (version & 0x0fff) as u16,
        }
    }
}

impl From<Version> for u32 {
    fn from(version: Version) -> Self {
        (version.major as u32) << 22 | ((version.minor & 0x03ff) as u32) << 12 | (version.patch & 0x0fff) as u32
    }
}
