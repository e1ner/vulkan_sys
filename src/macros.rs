macro_rules! enumeration {
    (
        $name:ident: $type: tt, {
            $($member_name:ident = $member_value:expr,)+ }
    ) => (
        #[derive(Clone, Copy, Debug, PartialEq)]
        #[repr($type)]
        pub enum $name {
            $($member_name = $member_value,)+
        }
    )
}

macro_rules! structure {
    (
        $(#[$attr: meta])*
        $name: ident, {
            $($member: ident: $member_ty: ty,)+}
    ) => {
        $(#[$attr])*
        #[repr(C)]
        pub struct $name {
            $(pub $member: $member_ty,)+
        }
    };

    (@IMPL
        $name: ident, {
            $($member: ident: $member_ty: ty,)+}
    ) => {
        unsafe impl Send for $name {}
        unsafe impl Sync for $name {}
    };
}

macro_rules! commands {
    (
        $label:ident, {
            $($name:ident => ($($param_name:ident: $param_type:ty),*) -> $ret:ty,)+ }
    ) => (
        pub struct $label {
            $(pub $name: extern "system" fn($($param_type),*) -> $ret,)+
        }

        impl fmt::Debug for $label {
            #[inline]
            fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
                writeln!(fmt, "")?;
                $(writeln!(fmt, "vk{}: 0x{:?}", stringify!($name), self.$name as *const usize)?;)+
                Ok(())
            }
        }

        unsafe impl Send for $label {}
        unsafe impl Sync for $label {}

        impl $label {
            pub fn load<F>(mut f: F) -> $label where F: FnMut(&CStr) -> *const c_void {
                $label {
                    $(
                        $name: unsafe {
                            extern "system" fn $name($(_: $param_type),*) {
                                panic!("function pointer '{}' not loaded", stringify!($name))
                            }
                            let name = CString::new(concat!("vk", stringify!($name)).to_owned()).unwrap();
                            let value = f(&name);
                            if value.is_null() { mem::transmute($name as *const ()) } else { mem::transmute(value) }
                        },
                    )+
                }
            }

            $(
                #[inline]
                pub unsafe fn $name(&self $(, $param_name: $param_type)*) -> $ret {
                    (self.$name)($($param_name),*)
                }
            )+
        }
    )
}
