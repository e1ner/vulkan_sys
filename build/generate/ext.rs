use {ContextGenerate, Target};
use parse::basic::Value;
use std::io::Write;
use utils::*;

/// Generates the constant definitions if not denied / protected.
pub fn generate(ctx: &ContextGenerate, target: &mut Target) {
    for variant in ctx.data
        .extensions
        .iter()
        .filter(|extension| extension.available)
        .flat_map(|extension| extension.required_variants.iter())
        .filter(|variant| variant.extends.is_none() && variant.value.is_some())
    {
        let (ty, value) = map_value(variant.value);
        outln!(target, "pub const {}: {} = {};", remove_prefix(variant.name, &["VK_"]), ty, value);
    }
}

/// Maps a value from the API registry into a rust conform value expression.
fn map_value<'a>(value: Option<Value<'a>>) -> (&'a str, String) {
    match value.unwrap() {
        Value::Value(value) => ("u32", format!("{}", value)),
        Value::String(string) => ("&'static str", format!("\"{}\"", string)),
        _ => panic!("unsupported value"),
    }
}
