use super::basic;
use {ContextGenerate, Target};
use std::io::Write;

/// Generates function pointer definitions.
pub fn generate(ctx: &ContextGenerate, target: &mut Target) {
    for function in ctx.data.functions.iter() {
        out!(target, "pub type {} = extern \"system\" fn(", function.name);
        for (index, parameter) in function.parameters.iter().enumerate() {
            basic::generate_parameter(target, parameter, index);
        }
        out!(target, ") -> ");
        basic::generate_return(target, &function.ret);
        outln!(target, ";");
    }
}
