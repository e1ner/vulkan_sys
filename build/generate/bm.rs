use super::utils;
use {ContextGenerate, Target};
use parse::Data;
use parse::ty::Type;
use std::io::Write;
use utils::*;

/// Generates bit masks and their type definitions if not denied / protected.
///
/// Bit masks are not generated directly but through the `bitmask` macro which gets all the
/// necessary information to finally build a bit mask with all the convenience necessary.
pub fn generate(ctx: &ContextGenerate, target: &mut Target) {
    use parse::basic::Value;
    use parse::en::Type;

    for bm in ctx.data
        .enums
        .iter()
        .filter(|en| en.ty == Type::Bitmask)
        .filter(|en| !utils::check_unavailable_type(ctx, en.name))
    {
        let variants = utils::merge_variants(ctx, bm);
        let mask_ty = find_type(bm.name, &ctx.data).unwrap();
        let name_no_tag = remove_suffix(utils::remove_tag(bm.name, &ctx.data), &["FlagBits"]);

        outln!(target, "bitmask! {{");
        outln!(target, "    #[repr(C)]");
        outln!(target, "    pub mask {}: u32 where flags {} {{", remove_prefix(mask_ty.name, &["Vk"]), remove_prefix(bm.name, &["Vk"]));
        for variant in variants.iter().filter(|variant| variant.value.is_some()) {
            outln!(
                target,
                "        {} = {},",
                fix_name(
                    variant.name,
                    name_no_tag,
                    match variant.value.as_ref().unwrap() {
                        &Value::BitPos(_) => "b",
                        &Value::Value(_) => "v",
                        x => panic!("not suppported variant value: {:?}", x),
                    },
                    &ctx.data
                ),
                match variant.value.as_ref().unwrap() {
                    &Value::BitPos(pos) => format!("0x{:x}", 1 << pos),
                    &Value::Value(value) => utils::format_value_hex(value),
                    x => panic!("not suppported variant value: {:?}", x),
                }
            );
        }
        outln!(target, "    }}");
        outln!(target, "}}");
    }
}

/// Fixes the name of a bit mask member (bit or value).
///
/// This is done by converting the name first into pascal case.
/// Then the name will be stripped of an optional tag and the 'Bit' suffix.
/// The bit mask name will also be removed from the front of the name. If not at least the 'Vk' prefix.
/// Finally the name will then get a new specified prefix and the optional tag as suffix reattached.
///
/// # Example
///
/// For the bit mask with name `VkDebugReportFlagBitsEXT`
/// a member name of 'VK_DEBUG_REPORT_INFORMATION_BIT_EXT' will be modified
/// into the name 'InformationEXT'.
fn fix_name(name: &str, name_group: &str, prefix: &str, data: &Data) -> String {
    use inflector::Inflector;

    let tag = utils::find_tag(name, data);

    let mut name = Inflector::to_pascal_case(name);
    if let Some(tag) = tag {
        let len = name.len() - tag.name.len();
        name.truncate(len);
    }

    let name = remove_prefix(remove_suffix(&name, &["Bit"]), &[name_group, "Vk"]);
    let mut name = prefix.to_owned() + &name;

    if let Some(tag) = tag {
        name.push_str(tag.name);
    }

    name
}

/// Finds the type definition of the bit mask.
fn find_type<'a>(name: &str, data: &'a Data) -> Option<&'a Type<'a>> {
    use parse::ty::Category;

    data.types
        .iter()
        .filter(|ty| ty.category == Category::Bitmask)
        .filter(|ty| match ty.requires {
            Some(requires) => requires == name,
            _ => false,
        })
        .next()
}
