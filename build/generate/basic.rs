use super::utils;
use Target;
use parse::basic::{Member, Type};
use std::io::Write;
use utils::*;

/// Generates a `Type` used in members and arguments.
pub fn generate_type(target: &mut Target, ty: &Type) {
    if ty.pointer {
        out!(target, "*{} ", if ty.constant { "const" } else { "mut" });
    }
    if ty.const_ptr {
        out!(target, "*const ");
    }
    let name = remove_prefix(utils::map_type(ty.name), &["Vk", "VK_"]);
    if let Some(size) = ty.size {
        out!(target, "[{}; {} as usize]", name, remove_prefix(size, &["VK_"]));
    } else {
        out!(target, "{}", name);
    }
}

/// Generates a return `Type` for functions (pointer and command).
pub fn generate_return(target: &mut Target, ty: &Type) {
    if ty.name != "void" || ty.pointer {
        generate_type(target, ty);
    } else {
        out!(target, "()");
    }
}

/// Generates a function (pointer and command) parameter / argument.
pub fn generate_parameter(target: &mut Target, parameter: &Member, index: usize) {
    if index > 0 {
        out!(target, ", ");
    }
    out!(target, "{}: ", utils::map_name(parameter.name));
    generate_type(target, &parameter.ty);
}
