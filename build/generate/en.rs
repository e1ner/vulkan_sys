use super::utils;
use {ContextGenerate, Target};
use parse::Data;
use std::io::Write;
use utils::*;

/// Generates enumerations and their type definitions if not denied / protected.
///
/// Enumerations are not generated directly but through the `enumeration` macro which gets all the
/// necessary information to finally build an enumeration with all the convenience necessary.
pub fn generate(ctx: &ContextGenerate, target: &mut Target) {
    use parse::basic::Value;
    use parse::en::Type;

    for en in ctx.data
        .enums
        .iter()
        .filter(|en| en.ty == Type::Enum)
        .filter(|en| !utils::check_unavailable_type(ctx, en.name))
    {
        let variants = utils::merge_variants(ctx, en);
        let name_no_tag = utils::remove_tag(en.name, &ctx.data);
        outln!(target, "enumeration!(");
        outln!(target, "    {}: {}, {{", remove_prefix(en.name, &["Vk"]), "u32");
        for variant in variants.iter().filter(|variant| variant.value.is_some()) {
            let value = match variant.value.as_ref().unwrap() {
                &Value::Value(v) => v as i32,
                &Value::Offset(o, _) => o as i32,
                x => panic!("{:?} no supported", x),
            };
            outln!(target, "        {} = {},", fix_name(variant.name, name_no_tag, "v", &ctx.data), utils::format_value(value));
        }
        outln!(target, "    }}");
        outln!(target, ");");
    }
}

/// Fixes the name of an enumeration member.
///
/// This is done by converting the name first into pascal case.
/// Then the name will be stripped of an optional tag suffix.
/// The enumeration name will also be removed from the front of the name. If not at least the 'Vk' prefix.
/// Finally the name will then get a new specified prefix and the optional tag as suffix reattached.
///
/// # Example
///
/// For the enumeration with name `VkPresentModeKHR`
/// a member name of 'VK_PRESENT_MODE_IMMEDIATE_KHR' will be modified
/// into the name 'ImmediateKHR'.
fn fix_name(name: &str, name_group: &str, prefix: &str, data: &Data) -> String {
    use inflector::Inflector;

    let tag = utils::find_tag(name, data);

    let mut name = Inflector::to_pascal_case(name);
    if let Some(tag) = tag {
        let len = name.len() - tag.name.len();
        name.truncate(len);
    }

    let name = remove_prefix(&name, &[name_group, "Vk"]).to_owned();
    let mut name = prefix.to_owned() + &name;

    if let Some(tag) = tag {
        name.push_str(tag.name);
    }

    name
}
