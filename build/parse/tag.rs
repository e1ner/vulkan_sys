use super::xpath;
use ContextExtract;
use std::fmt::{self, Debug};

/// A tag definition.
pub struct Tag<'a> {
    /// The name of the tag.
    pub name: &'a str,
    /// The author of the tag.
    pub author: &'a str,
    /// The contact of the author.
    pub contact: &'a str,
}

impl<'a> Debug for Tag<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}", self.name)
    }
}

/// Lists the tags specified in the API registry.
pub fn list<'a>(ctx: &'a ContextExtract) -> Vec<Tag<'a>> {
    xpath::evaluate(ctx.document().root(), "/registry/tags/tag")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .map(|element| {
            Tag {
                name: element.attribute_value("name").expect("tag/@name"),
                author: element.attribute_value("author").expect("tag/@author"),
                contact: element.attribute_value("contact").expect("tag/@contact"),
            }
        })
        .collect()
}
