use super::{utils, xpath};
use super::basic::{Variant, map_variant};
use ContextExtract;
use std::fmt::{self, Debug};
use sxd_document::dom::Element;

/// The enumeration types.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Type {
    /// An enumeration defining a bit mask.
    Bitmask,
    /// An enumeration defining variants (values).
    Enum,
}

/// An enumeration definition.
pub struct Enum<'a> {
    pub ty: Type,
    pub name: &'a str,
    pub comment: Option<&'a str>,
    pub variants: Vec<Variant<'a>>,
}

impl<'a> Debug for Enum<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:?} > {} -> {:?}", self.ty, self.name, self.variants)
    }
}

/// Lists the enumeration definitions defined in the API registry.
///
/// Both types, enumerations and bit masks, are listed.
///
/// The enumerations are also extended by the values defined in the extensions.
pub fn list<'a>(ctx: &'a ContextExtract) -> Vec<Enum<'a>> {
    xpath::evaluate(ctx.document().root(), "/registry/enums")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .filter_map(|element| map(element))
        .collect()
}

/// Maps an `enums` element into an `Enum` structure.
fn map(element: Element) -> Option<Enum> {
    let ty = match element.attribute_value("type") {
        Some(ty) => map_type(ty),
        _ => return None,
    };

    let variants = element
        .children()
        .into_iter()
        .filter_map(utils::child_element("enum"))
        .map(|e| map_variant(e, None))
        .collect();

    Some(Enum {
        ty: ty,
        name: element.attribute_value("name").expect("enums/@name"),
        comment: element.attribute_value("comment"),
        variants: variants,
    })
}

/// Maps the value of the `type` attribute into its appropriate `Type` variant.
///
/// Panics for unknown type values.
fn map_type(ty: &str) -> Type {
    match ty {
        "bitmask" => Type::Bitmask,
        "enum" => Type::Enum,
        _ => panic!("unknown enum type {}", ty),
    }
}
