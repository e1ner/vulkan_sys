use super::{utils, xpath};
use super::basic::{Member, Type};
use ContextExtract;
use regex::Regex;
use std::fmt::{self, Debug};
use sxd_document::dom::Element;

// A command definition.
pub struct Command<'a> {
    /// The name of the command.
    pub name: &'a str,
    /// The return type of the command.
    pub ret: Type<'a>,
    /// The arguments of the command.
    pub parameters: Vec<Member<'a>>,
}

impl<'a> Debug for Command<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "fn {} {:?} -> {:?}", self.name, self.parameters, self.ret)
    }
}

/// Lists the commands definitions specified in the API registry.
pub fn list<'a>(ctx: &'a ContextExtract) -> Vec<Command<'a>> {
    xpath::evaluate(ctx.document().root(), "/registry/commands/command")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .map(|element| map(element))
        .collect()
}

/// Maps a `command` element into the `Command` structure.
fn map(element: Element) -> Command {
    let proto = element
        .children()
        .into_iter()
        .filter_map(utils::child_element("proto"))
        .next()
        .expect("command/proto");
    let proto = map_parameter(proto);

    let parameters = element
        .children()
        .into_iter()
        .filter_map(utils::child_element("param"))
        .map(|e| map_parameter(e))
        .collect();

    Command { name: proto.name, ret: proto.ty, parameters: parameters }
}

/// Maps a `proto` or a `param` element into a `Member` structure.
fn map_parameter(element: Element) -> Member {
    let ty = element
        .children()
        .into_iter()
        .filter_map(utils::child_element("type"))
        .next()
        .expect("type");
    Member {
        name: utils::element_child_text(element, "name").expect("name"),
        ty: map_type(ty),
    }
}

/// Maps a `type` element into a `Type` structure.
fn map_type(element: Element) -> Type {
    let name = element
        .children()
        .into_iter()
        .filter_map(utils::child_text())
        .next()
        .expect("type");
    let constant = if let Some(captures) = element
        .preceding_siblings()
        .into_iter()
        .filter_map(utils::child_text())
        .last()
        .and_then(|t| RE_CONST.captures(t))
    {
        captures.get(1)
    } else {
        None
    };
    let after = element
        .following_siblings()
        .into_iter()
        .filter_map(utils::child_text())
        .next()
        .and_then(|t| RE_PTR_NAME.captures(t))
        .unwrap();

    Type {
        name: name,
        pointer: after.get(1).is_some(),
        constant: constant.is_some(),
        size: None,
        const_ptr: false,
    }
}
lazy_static! {
    static ref RE_CONST : Regex = Regex::new(r"\s*(const)\s+").unwrap();
    static ref RE_PTR_NAME : Regex = Regex::new(r"(\*)?\s*").unwrap();
}
