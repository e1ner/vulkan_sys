use super::xpath;
use ContextExtract;
use std::fmt::{self, Debug};
use sxd_document::dom::Element;

/// A constant value definition.
pub struct Const<'a> {
    /// The name of the constant.
    pub name: &'a str,
    /// The value of the constant.
    pub value: &'a str,
}

impl<'a> Debug for Const<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}: ? = {}", self.name, self.value)
    }
}

/// Lists the constant definitions specified in the API registry.
///
/// Constant definitions are specified in an `enum` element with the `name` attribute value `"API Constants"`.
pub fn list<'a>(ctx: &'a ContextExtract) -> Vec<Const<'a>> {
    xpath::evaluate(ctx.document().root(), "/registry/enums[@name = 'API Constants']/enum")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .map(|element| map(element))
        .collect()
}

/// Maps an `enum` element into a `Const` strucure.
fn map(element: Element) -> Const {
    Const {
        name: element.attribute_value("name").expect("enum/@name"),
        value: element.attribute_value("value").expect("enum/@value"),
    }
}
