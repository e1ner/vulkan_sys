use sxd_xpath::{Factory, Value, XPath};
use sxd_xpath::nodeset::{Node, Nodeset};

lazy_static! {
    static ref FACTORY: Factory = Factory::new();
}

/// Builds an XPath expression.
pub fn build(expression: &str) -> XPath {
    FACTORY.build(expression).unwrap().unwrap()
}

/// Evaluates an XPath expression.
pub fn evaluate_expression<'a, N: Into<Node<'a>>>(node: N, expression: &XPath) -> Option<Value<'a>> {
    use sxd_xpath::context::Context;

    let context = Context::new();
    expression.evaluate(&context, node).ok()
}

/// Evaluates a xpath expression.
pub fn evaluate<'a, N: Into<Node<'a>>>(node: N, expression: &str) -> Option<Value<'a>> {
    evaluate_expression(node, &build(expression))
}

/// Creates a closure to filter / map an xpath result value to a nodeset.
pub fn value_as_nodeset() -> impl FnMut(Value) -> Option<Nodeset> {
    |value| match value {
        Value::Nodeset(set) => Some(set),
        _ => None,
    }
}
