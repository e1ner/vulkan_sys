use std::fmt::{self, Debug};
use sxd_document::dom::Element;

/// A type definition.
pub struct Type<'a> {
    /// Name of the type.
    pub name: &'a str,
    /// If `true` the type is pointer type.
    pub pointer: bool,
    /// If `true` the type is a contant value.
    pub constant: bool,
    /// The optional size of the type.
    pub size: Option<&'a str>,
    /// If `true` it is a constant pointer of the type.
    pub const_ptr: bool,
}

impl<'a> Debug for Type<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}{}{}",
            if self.pointer {
                if self.constant {
                    "*const "
                } else {
                    "*mut "
                }
            } else {
                ""
            },
            if self.const_ptr { "*const " } else { "" },
            self.name
        )
    }
}

/// A member definition.
///
/// It might either be a member of a structure or a function argument.
pub struct Member<'a> {
    /// The name of the member.
    pub name: &'a str,
    /// The type of the member.
    pub ty: Type<'a>,
}

impl<'a> Debug for Member<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}: {:?}", self.name, self.ty)
    }
}

/// A variant value.
#[derive(Clone, Copy, PartialEq)]
pub enum Value<'a> {
    /// A bit position.
    BitPos(u32),
    /// A numeric value.
    Value(i32),
    /// A string value.
    String(&'a str),
    /// An offset value (offset, direction).
    Offset(u32, i32),
}

impl<'a> Debug for Value<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Value::BitPos(pos) => write!(fmt, "b{:b}", 1 << pos),
            &Value::Value(value) => write!(fmt, "{}", value),
            &Value::String(string) => write!(fmt, "'{}'", string),
            &Value::Offset(offset, direction) => write!(fmt, "{}/{}", offset, direction),
        }
    }
}

/// An enumeration variant.
pub struct Variant<'a> {
    /// The name of the variant.
    pub name: &'a str,
    /// The value of the variant.
    pub value: Option<Value<'a>>,
    /// The optional enumeration to extend.
    pub extends: Option<&'a str>,
    /// The optional comment.
    pub comment: Option<&'a str>,
}

impl<'a> Debug for Variant<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{} = {:?}", self.name, self.value)
    }
}

/// Maps an enumeration variant (value).
pub fn map_variant(element: Element, reference: Option<u32>) -> Variant {
    use super::utils::{i32_from_str, u32_from_str};

    let name = element.attribute_value("name");
    let value = element
        .attribute_value("value")
        .map(|value| if value.starts_with("\"") && value.ends_with("\"") {
            Value::String(&value[1..value.len() - 1])
        } else {
            i32_from_str(value).map(|v| Value::Value(v)).unwrap_or(Value::String(value))
        })
        .or_else(|| {
            element
                .attribute_value("bitpos")
                .map(|pos| Value::BitPos(u32_from_str(pos).unwrap()))
        })
        .or_else(|| {
            let dir = element
                .attribute_value("dir")
                .map(|dir| match dir {
                    "-" => -1,
                    x => panic!(format!("unknown direction '{}'", x)),
                })
                .unwrap_or(1);
            element
                .attribute_value("offset")
                .map(|offset| u32_from_str(offset).unwrap())
                .map(|offset| {
                    reference
                        .map(|r| Value::Value((r as i32 + offset as i32) * dir))
                        .unwrap_or(Value::Offset(offset, dir))
                })
        });

    Variant {
        name: name.expect("enum/@name"),
        value: value,
        extends: element.attribute_value("extends"),
        comment: element.attribute_value("comment"),
    }
}
