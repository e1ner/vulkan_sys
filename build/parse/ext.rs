use super::basic::{Variant, map_variant};
use super::xpath;
use ContextExtract;
use std::fmt::{self, Debug};
use sxd_document::dom::Element;

/// The extension types.
pub enum Type {
    Instance,
    Device,
}

/// The extension definition.
pub struct Extension<'a> {
    pub name: &'a str,
    pub number: u32,
    pub ty: Option<Type>,
    pub required_variants: Vec<Variant<'a>>,
    pub required_types: Vec<&'a str>,
    pub required_commands: Vec<&'a str>,
    pub required_extensions: Vec<&'a str>,
    pub protect: Option<&'a str>,
    pub supported: &'a str,
    pub available: bool,
    pub author: Option<&'a str>,
    pub contact: Option<&'a str>,
}

impl<'a> Debug for Extension<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}", self.name)
    }
}

/// Lists the extension definitions defined in the API registry.
pub fn list<'a>(ctx: &'a ContextExtract) -> Vec<Extension<'a>> {
    let mut extensions = xpath::evaluate(ctx.document().root(), "/registry/extensions/extension")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .map(|element| map(element))
        .collect::<Vec<Extension>>();
    mark_available(ctx, &mut extensions);
    extensions
}

/// Marks extensions whether they are available or not.
fn mark_available(ctx: &ContextExtract, extensions: &mut Vec<Extension>) {
    let mut names_unavailable = extensions
        .iter()
        .filter(|extension| unavailable_by_supported_and_protect(ctx, extension))
        .map(|extension| extension.name.to_owned())
        .collect::<Vec<String>>();

    let mut names = unavailable_by_dependency(extensions, &names_unavailable);
    while !names.is_empty() {
        names_unavailable.extend(names);
        names = unavailable_by_dependency(extensions, &names_unavailable);
    }

    for extension in extensions
        .iter_mut()
        .filter(|extension| !names_unavailable.iter().any(|name| name == extension.name))
    {
        extension.available = true;
    }
}

/// Determines if an extension is unavailable due to the `supported` and the `protected` value.
fn unavailable_by_supported_and_protect(ctx: &ContextExtract, extension: &Extension) -> bool {
    if extension.supported != "vulkan" {
        return true;
    }
    if let Some(protect) = extension.protect {
        if !ctx.defines.iter().any(|name| name == protect) {
            return true;
        }
    }
    false
}

/// Determines if an extension is unavailable due to extensions depended on.
fn unavailable_by_dependency<'a>(extensions: &'a Vec<Extension>, names_unavailable: &Vec<String>) -> Vec<String> {
    extensions
        .iter()
        .filter(|extension| !names_unavailable.iter().any(|name| name == extension.name))
        .filter(|extension| {
            extension
                .required_extensions
                .iter()
                .any(|required| names_unavailable.iter().any(|name| name == required))
        })
        .map(|extension| extension.name.to_owned())
        .collect()
}

/// Maps an `extension` element into an `Extension` structure.
fn map(element: Element) -> Extension {
    use std::str::FromStr;

    let number = element
        .attribute_value("number")
        .and_then(|n| u32::from_str(n).ok())
        .expect("extension/@number");
    let variants = xpath::evaluate(element, "require/enum")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .map(|element| map_variant(element, Some(1000000000 + (number - 1) * 1000)))
        .collect();
    let types = xpath::evaluate(element, "require/type")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .filter_map(|element| element.attribute_value("name"))
        .collect();
    let commands = xpath::evaluate(element, "require/command")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .filter_map(|element| element.attribute_value("name"))
        .collect();

    Extension {
        name: element.attribute_value("name").expect("extension/@name"),
        number: number,
        ty: map_type(element.attribute_value("type")),
        required_variants: variants,
        required_types: types,
        required_commands: commands,
        required_extensions: map_required_extensions(element.attribute_value("requires")),
        protect: element.attribute_value("protect"),
        supported: element.attribute_value("supported").expect("extension/@supported"),
        available: false,
        author: element.attribute_value("author"),
        contact: element.attribute_value("contact"),
    }
}

/// Maps the `type` attribute into the `Type` enum.
fn map_type(value: Option<&str>) -> Option<Type> {
    value.map(|value| match value {
        "device" => Type::Device,
        "instance" => Type::Instance,
        x => panic!(format!("unknown extension/@type = '{}'", x)),
    })
}

/// Maps the requirements.
fn map_required_extensions(value: Option<&str>) -> Vec<&str> {
    match value {
        Some(value) => value.split(",").collect(),
        _ => Default::default(),
    }
}
