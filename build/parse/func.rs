use super::{utils, xpath};
use super::basic::{Member, Type};
use ContextExtract;
use regex::Regex;
use std::fmt::{self, Debug};
use sxd_document::dom::Element;
use utils::*;

/// A function pointer definition.
pub struct Function<'a> {
    /// The name of the function pointer.
    pub name: &'a str,
    /// The return type of the function pointer.
    pub ret: Type<'a>,
    /// The arguments of the function pointer.
    pub parameters: Vec<Member<'a>>,
}

impl<'a> Debug for Function<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "fn {} {:?} -> {:?}", self.name, self.parameters, self.ret)
    }
}

/// Lists all function pointer definitions specified in the API registry.
///
/// Function pointer are specified each as `type` element with the `category` attribute value `"funcpointer"`.
pub fn list<'a>(ctx: &'a ContextExtract) -> Vec<Function<'a>> {
    xpath::evaluate(ctx.document().root(), "/registry/types/type[@category = 'funcpointer']")
        .and_then(xpath::value_as_nodeset())
        .iter()
        .flat_map(|set| set.iter())
        .filter_map(|node| node.element())
        .map(|element| map(element))
        .collect()
}

/// Maps a `type` element into a `Function` structure.
fn map(element: Element) -> Function {
    let parameters = element
        .children()
        .into_iter()
        .filter_map(utils::child_element("type"))
        .map(|e| map_parameter(e))
        .collect();

    Function {
        name: utils::element_child_text(element, "name").expect("type/name"),
        ret: map_ret(element),
        parameters: parameters,
    }
}

/// Maps the return type into a `Type` structure.
fn map_ret(element: Element) -> Type {
    let text = element
        .children()
        .into_iter()
        .filter_map(|c| c.text())
        .map(|t| t.text())
        .next()
        .expect("type/");
    let text = remove_prefix(text, &["typedef "]);
    let (text, constant) = if text.starts_with("const") {
        (remove_prefix(text, &["const "]), true)
    } else {
        (text, false)
    };
    let text = text.split_whitespace().next().expect("return type");
    let (ty, pointer) = if text.ends_with("*") {
        (remove_suffix(text, &["*"]), true)
    } else {
        (text, false)
    };

    Type { name: ty, pointer: pointer, constant: constant, size: None, const_ptr: false }
}

/// Maps an argument into a `Member` structure.
fn map_parameter(element: Element) -> Member {
    let ty = element
        .children()
        .into_iter()
        .filter_map(utils::child_text())
        .next()
        .expect("text/");
    let constant = if let Some(captures) = RE_CONST.captures(
        element
            .preceding_siblings()
            .into_iter()
            .filter_map(utils::child_text())
            .last()
            .unwrap(),
    ) {
        captures.get(1)
    } else {
        None
    };
    let after = element
        .following_siblings()
        .into_iter()
        .filter_map(utils::child_text())
        .next()
        .unwrap();
    let after = RE_PTR_NAME.captures(after).expect(after);

    Member {
        name: after.get(2).unwrap().as_str(),
        ty: Type {
            name: ty,
            pointer: after.get(1).is_some(),
            constant: constant.is_some(),
            size: None,
            const_ptr: false,
        },
    }
}
lazy_static! {
    static ref RE_CONST : Regex = Regex::new(r"\s*(const)\s+").unwrap();
    static ref RE_PTR_NAME : Regex = Regex::new(r"(\*)?\s+(\w+)").unwrap();
}
