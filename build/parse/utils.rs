use std::num::ParseIntError;
use sxd_document::dom::{ChildOfElement, Element};

/// Creates a filter/map closure to map a text-child to a string.
pub fn child_text() -> impl FnMut(ChildOfElement) -> Option<&str> {
    |child| child.text().map(|t| t.text())
}

/// Creates a closure to map an element-child to an element and filter for a specific element-name.
pub fn child_element<'a>(name: &'a str) -> impl FnMut(ChildOfElement<'a>) -> Option<Element<'a>> {
    move |child| {
        child.element().and_then(|element| if element.name().local_part() == name {
            Some(element)
        } else {
            None
        })
    }
}

/// Gets the text from a named child elements.
pub fn element_child_text<'a>(element: Element<'a>, name: &'a str) -> Option<&'a str> {
    element
        .children()
        .into_iter()
        .filter_map(child_element(name))
        .next()
        .iter()
        .filter_map(|element| element.children().into_iter().filter_map(child_text()).next())
        .next()
}

/// Converts a string into an u32 value considering the two possible radixes 10 and 16.
pub fn u32_from_str(string: &str) -> Result<u32, ParseIntError> {
    if string.starts_with("0x") {
        u32::from_str_radix(&string[2..], 16)
    } else {
        u32::from_str_radix(string, 10)
    }
}

/// Converts a string into an i32 value considering the two possible radixes 10 and 16.
pub fn i32_from_str(string: &str) -> Result<i32, ParseIntError> {
    if string.starts_with("0x") {
        i32::from_str_radix(&string[2..], 16)
    } else if string.starts_with("-0x") {
        i32::from_str_radix(&string[3..], 16).map(|v| -v)
    } else {
        i32::from_str_radix(string, 10)
    }
}
