mod xpath;
mod utils;
pub mod basic;
pub mod tag;
pub mod ext;
pub mod ty;
pub mod cn;
pub mod en;
pub mod func;
pub mod st;
pub mod cmd;

use ContextExtract;
use std::fs::File;
use std::io::Read;
use sxd_document::Package;

/// Reads the XML specified with `path` into a DOM.
pub fn read_xml(path: &str) -> Package {
    use sxd_document::parser;

    let mut file = File::open(path).unwrap();
    let mut xml = Default::default();
    file.read_to_string(&mut xml).unwrap();
    parser::parse(xml.as_str()).expect("parsing XML")
}

/// Extract the required data to generate the API from the DOM.
pub fn extract_data<'a>(ctx: &'a ContextExtract) -> Data<'a> {
    Data {
        tags: tag::list(ctx),
        extensions: ext::list(ctx),
        types: ty::list(ctx),
        consts: cn::list(ctx),
        enums: en::list(ctx),
        functions: func::list(ctx),
        structs: st::list(ctx),
        commands: cmd::list(ctx),
    }
}

/// The extracted data required to generate the API.
pub struct Data<'a> {
    pub tags: Vec<tag::Tag<'a>>,
    pub extensions: Vec<ext::Extension<'a>>,
    pub types: Vec<ty::Type<'a>>,
    pub consts: Vec<cn::Const<'a>>,
    pub enums: Vec<en::Enum<'a>>,
    pub functions: Vec<func::Function<'a>>,
    pub structs: Vec<st::Struct<'a>>,
    pub commands: Vec<cmd::Command<'a>>,
}
