extern crate shared_library;
extern crate vulkan_sys;

use shared_library::dynamic_library::DynamicLibrary;
use vulkan_sys::{CommandsExported, CommandsGlobal, ExtensionProperties, LayerProperties};

fn main() {
    use std::ffi::CStr;

    let library = open_library();
    let cmd_exported = get_commands_exported(&library);
    let cmd_global = get_commands_global(&cmd_exported);

    for layer in list_supported_instance_layers(&cmd_global) {
        println!("inst-layer: {}", unsafe { CStr::from_ptr(layer.layerName.as_ptr()).to_str().unwrap() });
    }
    for extension in list_supported_instance_extensions(&cmd_global) {
        println!("inst-extension: {}", unsafe { CStr::from_ptr(extension.extensionName.as_ptr()).to_str().unwrap() });
    }
}

/// Lists supported instance extensions.
fn list_supported_instance_extensions(global: &CommandsGlobal) -> Vec<ExtensionProperties> {
    use std::ptr;

    let mut count = Default::default();
    let _ = unsafe { global.EnumerateInstanceExtensionProperties(ptr::null(), &mut count, ptr::null_mut()) };

    let mut properties = Vec::with_capacity(count as usize);
    let _ = unsafe { global.EnumerateInstanceExtensionProperties(ptr::null(), &mut count, properties.as_mut_ptr()) };
    unsafe { properties.set_len(count as usize) };

    properties
}

/// Lists supported instance layers.
fn list_supported_instance_layers(global: &CommandsGlobal) -> Vec<LayerProperties> {
    use std::ptr;

    let mut count = Default::default();
    let _ = unsafe { global.EnumerateInstanceLayerProperties(&mut count, ptr::null_mut()) };

    let mut properties = Vec::with_capacity(count as usize);
    let _ = unsafe { global.EnumerateInstanceLayerProperties(&mut count, properties.as_mut_ptr()) };
    unsafe { properties.set_len(count as usize) };

    properties
}

/// Get the global commands.
fn get_commands_global(cmd_exported: &CommandsExported) -> CommandsGlobal {
    use std::mem;

    CommandsGlobal::load(|name| unsafe { mem::transmute(cmd_exported.GetInstanceProcAddr(0, name.as_ptr())) })
}

/// Get the exported commands.
fn get_commands_exported(library: &DynamicLibrary) -> CommandsExported {
    use std::ptr;

    CommandsExported::load(|name| unsafe {
        match library.symbol(name.to_str().unwrap()) {
            Ok(s) => s,
            Err(_) => ptr::null(),
        }
    })
}

/// Open the Vulkan loader library.
fn open_library() -> DynamicLibrary {
    use std::path::Path;

    #[cfg(windows)]
    fn get_path() -> &'static Path {
        Path::new("vulkan-1.dll")
    }

    #[cfg(all(unix, not(target_os = "android"), not(target_os = "macos")))]
    fn get_path() -> &'static Path {
        Path::new("libvulkan.so.1")
    }

    #[cfg(target_os = "android")]
    fn get_path() -> &'static Path {
        Path::new("libvulkan.so")
    }

    DynamicLibrary::open(Some(get_path())).unwrap()
}
